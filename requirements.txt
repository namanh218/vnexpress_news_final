bs4==0.0.1 
beautifulsoup4==4.9.0
pymongo==3.10.1
pip==20.1
flask==1.1.2
Flask-Login==0.5.0
celery==4.4.2
