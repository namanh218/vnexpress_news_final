import bcrypt

from flask import Flask, request, jsonify, session, redirect, url_for
from config import collection, client

app = Flask(__name__)
app.config.from_pyfile('config.py')


@app.route('/')
def home():
    return 'Hello Man'


@app.route('/all-news', methods=['GET'])
def all_news():
    output = []

    for i in collection.find():
        output.append({'title': i['title'], 'Nội dung': i['content']})

    return jsonify({'result': output})


@app.route('/all-news/<news>', methods=['GET'])
def find_news(news):
    res = collection.find_one({'title': news})

    if res:
        output = {'title': res['title'], 'Nội Dung': res['content']}

    else:
        output = 'Not found this post'

    return jsonify({'result': output})


@app.route('/delete-news/<news>', methods=['DELETE'])
def del_news(news):
    if 'username' in session:
        collection.delete_one({'title': news})
        output = collection.find_one({'title': news})

        if output:
            return jsonify({'result': 'not success'})
        else:
            return jsonify({'result': 'success'})

    return redirect(url_for('login'))


@app.route('/update-news/<news>', methods=['PUT'])
def update_news(news):
    if 'username' in session:
        collection.update_one({'title': news}, {'$set': {'content': request.form['new_content']}})
        output = collection.find_one({'title': news})
        return jsonify({'result': output})

    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        db = client['users']
        users = db.user
        if '{}' in request.form['username']:
            return 'There are strange character in here'
        login_user = users.find_one({'name': request.form['username'].strip('$')})

        if login_user:
            if bcrypt.hashpw(request.form['password'].encode('utf-8'), login_user['password']) == login_user['password']:
                session['username'] = request.form['username']
                return 'Welcome back!'
        return jsonify({'result': 'Invalid user or password'})
    else:
        return 'Login please!'


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        db = client['users']
        users = db.user
        if '{}' in request.form['username']:
            return 'There are strange character in here'

        exist_user = users.find_one({'name': request.form['username'].strip('$')})

        if not exist_user:
            hash_pass = bcrypt.hashpw(request.form['password'].encode('utf-8'), bcrypt.gensalt())
            users.insert_one({'name': request.form['username'].strip('$'), 'password': hash_pass})
            session['username'] = request.form['username']
            return jsonify({'result': 'Hi there!'})
        return jsonify({'result': 'This account was exists'})
    else:
        return 'Welcome to page'


@app.route('/logout')
def logout():
    session.pop('username')
    return jsonify({'result': 'Sign out'})


@app.route('/news/page/<int:page>', methods=['GET'])
def pagination(page):
    page_size = 38
    skips = page_size * (page - 1)
    cursor = collection.find().skip(skips).limit(page_size)
    value_ = [i for i in cursor]
    return jsonify(value_)


if __name__ == '__main__':
    app.secret_key = 'abc'
    app.run(host='0.0.0.0', port=8000)

